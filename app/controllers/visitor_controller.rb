class VisitorController < ApplicationController
  def home
  end

  def about_us
  end

  def company_history
  end

  def blog
    @blogs = Blog.all
  end

  def contact_us
  end
end
